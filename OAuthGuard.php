<?php

namespace YqueueSocialite;

use Laravel\Socialite\Two\User;

class OAuthGuard
{
    const SESSION_USER_KEY = '_oauth.user';

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return session()->has(self::SESSION_USER_KEY);
    }

    /**
     * Get the currently authenticated user.
     *
     * @return User|null
     */
    public function user()
    {
        return session()->get(self::SESSION_USER_KEY);
    }

    /**
     * Set the current user.
     *
     * @param User $user
     * @return void
     */
    public function setUser(User $user)
    {
        session()->put(self::SESSION_USER_KEY, $user);
    }

    /**
     * Log the current user out of their session.
     *
     * @return void
     */
    public function logout()
    {
        session()->remove(self::SESSION_USER_KEY);
    }
}