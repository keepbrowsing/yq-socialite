<?php

namespace YqueueSocialite;

use Closure;
use Illuminate\Auth\AuthenticationException;

class OAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {
        if (! oauth()->check()) {
            throw new AuthenticationException(
                'Unauthenticated.', [], '/oauth'
            );
        }

        return $next($request);
    }
}