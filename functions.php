<?php

use YqueueSocialite\OAuthGuard;

if (! function_exists('oauth')) {

    /**
     * Convenience function to retrieve OAuth guard.
     *
     * @return OAuthGuard
     */
    function oauth()
    {
        return app(OAuthGuard::class);
    }
}