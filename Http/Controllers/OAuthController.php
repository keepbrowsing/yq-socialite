<?php

namespace YqueueSocialite\Http\Controllers;

use YqueueSocialite\Http\Controllers\Controller;
use Socialite;

class OAuthController extends Controller
{
    const SESSION_REFERER_KEY = '_oauth.referer';

    /**
     * Redirect to the OAuth provider.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        // Temporarily store the referer so we can go back there once auth'd
        session()->flash(self::SESSION_REFERER_KEY, url()->previous());

        return Socialite::driver('yqueue')->redirect();
    }

    /**
     * Handle the callback from the OAuth provider.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleCallback()
    {
        // Set the user (if accepted)
        oauth()->setUser(
            Socialite::driver('yqueue')->user()
        );

        return redirect()->to(
            session()->get(self::SESSION_REFERER_KEY)
        );
    }

    /**
     * Provides the ability to logout from the account.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        oauth()->logout();

        return redirect()->back();
    }
}
